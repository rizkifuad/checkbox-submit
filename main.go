package main

import (
	"log"
	"os"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/template/html"
)

func main() {
	engine := html.New("./views", ".html")

	app := fiber.New(fiber.Config{
		Views:       engine,
		ViewsLayout: "layouts/main",
	})

	app.Get("/", func(c *fiber.Ctx) error {
		return c.Render("index", fiber.Map{
			"hello": "world",
		})
	})

	app.Post("/", func(c *fiber.Ctx) error {
		return c.JSON(fiber.Map{
			"username":   c.FormValue("username"),
			"statusC-1":  c.FormValue("statusC-1"),
			"statusC-2":  c.FormValue("statusC-2"),
			"statusC-3":  c.FormValue("statusC-3"),
			"statusC-4":  c.FormValue("statusC-4"),
			"statusC-5":  c.FormValue("statusC-5"),
			"submitName": c.FormValue("submitName"),
		})
	})

	port := os.Getenv("PORT")
	if port == "" {
		port = "5000"
	}

	port = ":" + port

	log.Fatal(app.Listen(port))
}
